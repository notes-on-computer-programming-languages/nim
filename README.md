# nim

Imperative, general-purpose, multi-paradigm, statically typed, systems, compiled. http://nim-lang.org/

* [Nim_(programming_language)](https://en.m.wikipedia.org/wiki/Nim_(programming_language))
* [*Nim in action*
  ](https://www.worldcat.org/search?q=ti%3Anim+in+action)
  2017 Dominik Picheta
* [Curated Packages](https://github.com/nim-lang/Nim/wiki/Curated-Packages)
